# Target architectures

TuxMake supports building for a set of architectures, and they are documented
here in alphabetical order. For each architecture, we specify which toolchains
are used

Architecture | Aliases     | Description
-------------|-------------|------
arc          |             | ARC
arm64        | *aarch64*   | 64-bit ARMv8
arm          |             | 32-bit ARM
i386         |             | 32-bit X86
mips         |             | 32-bit MIPS
openrisc     |             | OpenRISC
parisc       |             | 64-bit parisc
powerpc      |             | 64-bit PowerPC (EL)
riscv        |             | 64-bit RISC-V
s390         |             | 64-bit IBM S/390
sh           |             | 32-bit sh4
sparc        |             | 64-bit Sparc
um           |             | User-Mode Linux
x86_64       | *amd64*     | 64-bit X86
