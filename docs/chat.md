# Chat

Please feel free to join our chat channels to collaborate with us and with
other TuxMake users.

## IRC

You can talk to the TuxMake team at the `#tuxmake` channel on Freenode.

## Discord

There is also a [Discord instance for TuxSuite](https://discord.gg/4hhTzUrj5M),
where there is a `#tuxmake` channel.
