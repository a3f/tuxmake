[TuxMake](https://tuxmake.org) containers, by [Linaro](https://linaro.org),
provide pristine and shareable environments for building Linux kernels.

The source and build procedure for the containers can be found at in the
[tuxmake gitlab repository](https://gitlab.com/Linaro/tuxmake).

There is a TuxMake container for every supported combination of target
architecture and toolchain - over 100 in all. The naming structure for
containers is `tuxmake/[target architecture]_[toolchain]-[version]`

All containers are hosted on both [docker
hub](https://hub.docker.com/u/tuxmake) and [Amazon ECR Public
Gallery](https://gallery.ecr.aws/?searchTerm=tuxmake).
